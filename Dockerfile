FROM python:alpine 

WORKDIR /app

COPY requirments.txt .
RUN python -m pip install --upgrade pip && pip install -r requirments.txt

COPY . .

CMD ['python','app.py']